import React from 'react';
import Layout from 'antd/es/layout';
import { BrowserRouter } from 'react-router-dom';

import { Header as AppHeader, Menu } from './components/Header';
import { Yield } from './components/Router';
import Routes from './router';
import './App.css';

const {
  Header,
  Content
} = Layout;

function App() {

  return (
    <BrowserRouter >
      <Layout style={{ height: '100%', width: '100%', maxWidth: 1144, margin: 'auto', backgroundColor: 'transparent' }}>
        <Header>
          <AppHeader height={64} iconUrl="#" baseUrl='/' title="the title" >
            <Menu
              routes={[
                { path: '/', label: 'Home' },
                { path: '/', label: 'Publications' },
                { path: '/login', label: 'Sigin In' },
                { path: '/sigin-up', label: 'Register' },
              ]}
            >
              <span>
                Content
              </span>
            </Menu>
          </AppHeader>
        </Header>
        <Content>
          {/* Yield */}
          <Yield routes={Routes} />
        </Content>
      </Layout>
    </BrowserRouter>
  )
}

export default App;
