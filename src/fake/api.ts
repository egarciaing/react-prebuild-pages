import { loremIpsum } from 'lorem-ipsum';
import {
    ItemData,
    CoverData,
    Owner
} from '../components/CommonTypes';

const cover: CoverData = {
    height: 824,
    width: 768,
    src: 'http://localhost:8080/tkmem-api/uploads/food/c49983831864a815e4cc7fbc8498f46e82618b14.jpeg',
    placeholder: 'http://localhost:8080/tkmem-api/uploads/food/ee3cc1e61669ad685adedc96e257e048ee34ce75.jpeg'
};

const owner: Owner = {
    _id: '12332d32',
    firstNames: 'Ezequiel Nehemia',
    lastNames: 'Garcia Hernandez',
    username: 'egarcia',
    fullName: 'Ezequiel Nehemia Garcia Hernandez',
    address: {
        email: 'email@domain.com',
        verified: false
    },
    aboutMe: loremIpsum({ count: 7, units: 'sentences' })
};

const item: ItemData<{ images: CoverData[] }> = {
    _id: 1,
    title: 'The Big Title Post',
    createdAt: (new Date).toISOString(),
    body: loremIpsum({ count: 10, units: 'sentences' }),
    cover: cover,
    mediaCount: 3,
    images: [cover, cover, cover],
    owner
};

const item2: ItemData<{ images: CoverData[] }> = {
    _id: 1,
    title: 'The Big Title Post',
    createdAt: (new Date).toISOString(),
    body: loremIpsum({ count: 10, units: 'sentences' }),
    mediaCount: 0,
    images: [cover, cover, cover],
    owner
};

export {
    item,item2, owner, cover
}