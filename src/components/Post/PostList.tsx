import React, { CSSProperties } from 'react';
import PropTypes from 'prop-types';
import Skeleton from 'antd/es/skeleton';
import Empty from 'antd/es/empty';
import {
    List,
    AutoSizer,
    WindowScroller,
    CellMeasurer,
    CellMeasurerCache,
    InfiniteLoader
} from 'react-virtualized';
import { ItemProps, ItemData } from '../CommonTypes';

type loadMoreFunc = (params: {
    startIndex: number,
    stopIndex: number,
    list: Array<ItemData>
}) => Promise<ItemData[]>;

type PostListProps = {
    list: Array<ItemData>;
    hasMore: number;
    overscan: number;
    style: CSSProperties;
    loadMoreHandler: loadMoreFunc;
    renderItem: (props: ItemProps & { index: number, registerChild: any }) => React.ReactNode;
    onItemClick?: (params: { index: number, loading: boolean, data: ItemData }) => void;
}

interface PostList {
    cache: CellMeasurerCache;
    state: {
        list: Array<ItemData>
        lastSelection: number | null
    };
    constructor(props: PostListProps);
}

const ItemWrapper: CSSProperties = {
    backgroundColor: '#fff',
    border: '1px solid #f0f0f0',
    paddingTop: '1rem'
}

class PostList extends React.PureComponent {

    static defaultProps = {
        hasMore: 2,
        overscan: 3
    }

    static propTypes = {
        hasMore: PropTypes.number,
        overscan: PropTypes.number,
        list: PropTypes.array.isRequired,
        style: PropTypes.object.isRequired,
        loadMoreHandler: PropTypes.func.isRequired,
        renderItem: PropTypes.func.isRequired,
        onItemClick: PropTypes.func
    }

    constructor(public props: PostListProps) {
        super(props);
        this.state = {
            list: props.list,
            lastSelection: null
        };

        this.cache = new CellMeasurerCache({
            fixedWidth: true,
            defaultHeight: 150,
            minHeight: 100
        });
    }

    renderRow = ({ index, parent, key, style }) => {

        const { list } = this.state;
        const { onItemClick, renderItem } = this.props;

        const data = list[index];
        const loading = !this.isRowLoaded({ index });
        const width = '100%';

        return (
            <CellMeasurer
                key={key}
                parent={parent}
                rowIndex={index}
                cache={this.cache}
                columnIndex={0}
            >
                {({ measure, registerChild }) => (

                    loading ?
                        (
                            <Skeleton active title loading avatar={{ size: 'large' }} paragraph={{ rows: 4 }} className='post-list item-skeleton' >
                                <div
                                    ref={el => registerChild && registerChild(el as any)}
                                    style={{ ...style, ...ItemWrapper }}
                                >
                                </div>
                            </Skeleton>
                        ) :
                        renderItem({
                            index,
                            width,
                            data,
                            registerChild,
                            measurer: measure,
                            onClick: () => onItemClick && onItemClick({ index, loading, data })
                        })
                )}
            </CellMeasurer>
        );
    }

    isRowLoaded = ({ index }) => {
        const { list } = this.state;
        return !!list[index];
    }

    internalHandleLoader = ({ startIndex, stopIndex }) => {
        const { loadMoreHandler } = this.props;
        const { list } = this.state;

        return loadMoreHandler({
            startIndex,
            stopIndex,
            list
        }).then(nextpage => {
            this.setState({ list: list.concat(nextpage) })
        }) as Promise<void>
    }

    render() {
        const { style, hasMore, overscan } = this.props;
        const { list } = this.state;
        const count = list.length + hasMore/* offset */;

        return (
            <WindowScroller>
                {({ isScrolling, onChildScroll, scrollTop, registerChild }) => (
                    <div ref={registerChild}>
                        <InfiniteLoader
                            rowCount={count}
                            isRowLoaded={this.isRowLoaded}
                            loadMoreRows={this.internalHandleLoader}
                        >
                            {({ onRowsRendered, registerChild: registerList }) => (
                                <div style={style}>
                                    <AutoSizer>
                                        {({ height, width }) => (
                                            <List
                                                ref={registerList}
                                                autoHeight
                                                width={width}
                                                height={height}

                                                scrollTop={scrollTop}
                                                onScroll={onChildScroll}
                                                isScrolling={isScrolling}
                                                onRowsRendered={onRowsRendered}

                                                deferredMeasurementCache={this.cache}
                                                rowHeight={this.cache.rowHeight}
                                                rowRenderer={this.renderRow}
                                                noRowsRenderer={() => <Empty description={false} />}
                                                overscanRowCount={overscan}
                                                rowCount={count}
                                            />
                                        )}
                                    </AutoSizer>
                                </div>
                            )}
                        </InfiniteLoader>
                    </div>
                )}
            </WindowScroller>

        );
    }
}

export default PostList;
