import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import MomentTypes from 'react-moment-proptypes';
import {
    Card,
    Avatar,
    Typography,
    Badge
} from 'antd';
import { EditFilled, PictureFilled } from '@ant-design/icons';
import { LazyImageFull, ImageState } from "react-lazy-images";
import { ItemProps, CoverData } from '../CommonTypes';

interface PostItem {
    new(props: ItemProps): PostItem;
}

const { Paragraph, Text } = Typography

function PostCover(props: { width: number | string, cover: CoverData, count: number, measurer: () => void }): React.ReactElement {
    const { width, cover, count, measurer } = props;

    return (
        <div className='cover'>
            <LazyImageFull src={cover.src}>
                {({ imageProps, imageState, ref }) => (
                    // eslint-disable-next-line jsx-a11y/alt-text
                    <img
                        {...imageProps}
                        ref={ref}
                        src={
                            imageState === ImageState.LoadSuccess
                                ? imageProps.src
                                : cover?.placeholder || '#'
                        }
                        onLoad={measurer}
                        style={{ height: cover.height, width: cover.width, maxWidth: width, maxHeight: '100%' }}
                    />
                )}
            </LazyImageFull>
            {
                count > 0 ?
                    (<span style={{ marginTop: -24, marginRight: 24, float: 'right' }}>
                        <Badge count={count} overflowCount={9} style={{ backgroundColor: '#fff', color: '#000' }} >
                            <PictureFilled style={{ fontSize: 24 }} />
                        </Badge>
                    </span>) :
                    null
            }
        </div>
    )
}
PostCover.propTypes = {
    cover: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    measurer: PropTypes.func.isRequired,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
}

class PostItem extends React.PureComponent {
    static defaultProps = {
        onClick: () => { },
        style: {
            flex: 1,
            marginBottom: 10
        }
    }

    static propTypes = {
        data: PropTypes.shape({
            _id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
            owner: PropTypes.object.isRequired,
            title: PropTypes.string.isRequired,
            text: PropTypes.string,
            createdAt: MomentTypes.momentString,
            cover: PropTypes.object,
            mediaCount: PropTypes.number,
        }),
        style: PropTypes.object,
        width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
        measurer: PropTypes.func.isRequired,
        onClick: PropTypes.func
    };

    constructor(public props: ItemProps) {
        super(props);
    }
    
    render() {
        const { width, data, measurer, style, onClick } = this.props;
        const { _id, owner, createdAt, body, title, mediaCount, cover = null } = data;
        const { avatar, fullName, username } = owner;

        return (
            <Card hoverable key={_id} style={{ width, ...style }} onClick={() => onClick && onClick(data)}>
                <Card.Meta
                    avatar={<Avatar src={avatar} alt={username} size='large'>{username}</Avatar>}
                    title={<Text title={`@${username}`} >{fullName || username}</Text>}
                    description={<div> <EditFilled /> <Moment date={createdAt} fromNow ago /> </div>}
                />
                {
                    !!cover ?
                        <PostCover
                            measurer={measurer}
                            count={mediaCount || 0}
                            width={'100%'}
                            cover={cover}
                        /> :
                        null
                }
                <Text type='secondary' strong>{title}</Text>
                <Paragraph ellipsis={{ rows: 4 }} style={{ marginTop: 14 }}>
                    {body}
                </Paragraph>
            </Card>
        );
    }
}

export default PostItem;
