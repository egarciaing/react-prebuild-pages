import React, { CSSProperties } from 'react';
import PropTypes from 'prop-types';
import {
    Card,
    Col, Row,
    Typography,
    Skeleton
} from 'antd';
import {
    CloseCircleFilled
} from '@ant-design/icons'
import Slider from 'react-slick';
import { LazyImageFull, ImageState } from "react-lazy-images";
import {
    ItemData,
    CoverData,
} from '../CommonTypes';
import PostHeader from '../Profile/ProfileHeader';

type PostDetailsProps = {
    data: ItemData<{ images?: Array<CoverData> }>
    loading: boolean
    style?: CSSProperties
    onClose?: () => void
}

interface PostDetails {
    props: PostDetailsProps;
    carousel: Slider | null;
    new(props: PostDetailsProps): PostDetails;
}

const RowStyle: CSSProperties = {
    flex: 1,
    width: '100%',
    height: '100%'
}

const ColStyle: CSSProperties = {
    overflow: 'auto',
    height: '100%',
    flex: 1
}

const SliderSettings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    lazyLoad: true,
}

function CustomSlide(props: { cover: CoverData, [key: string]: any }): React.ReactElement {
    const { cover, heightWrapper, ...config } = props;
    const { width, height, src, placeholder } = cover;

    return (
        <div {...config}>
            <div style={{ height: heightWrapper, width: '100%', maxHeight: '100%', display: 'block', backgroundColor: 'rgba(0,0,0,0.66)' }}>
                <LazyImageFull src={src}>
                    {({ imageProps, imageState, ref }) => (
                        // eslint-disable-next-line jsx-a11y/alt-text
                        <img
                            {...imageProps}
                            ref={ref}
                            src={
                                imageState === ImageState.LoadSuccess
                                    ? imageProps.src
                                    : placeholder || '#'
                            }
                            style={{ height: height, width: width, maxWidth: '100%', maxHeight: '100%' }}
                        />
                    )}
                </LazyImageFull>
            </div>
        </div>
    )
}
CustomSlide.propTypes = {
    cover: PropTypes.object.isRequired,
}


class PostDetails extends React.Component {
    static defaultPrrops = {
        loading: false
    }

    static propTypes = {
        style: PropTypes.object,
        loading: PropTypes.bool,
        onClose: PropTypes.func,
        data: PropTypes.object.isRequired
    }

    constructor(public props: PostDetailsProps) {
        super(props)
    }

    getBoundsFromStyle(style: CSSProperties | undefined) {
        var { width = '100%', height = 400 } = style || {};
        height = typeof height === 'string' ? `calc(${height} - 60px)` : height - 60;
        return { width, height };
    }

    render() {
        const { style, data, loading, onClose } = this.props;
        const { owner, images, body, title, createdAt } = data;
        const { height } = this.getBoundsFromStyle(style);

        return (
            <div className="post-details" style={style}>
                <Row gutter={[8, 0]} justify='space-around' align='top' style={RowStyle}>
                    <Col
                        flex='1 1 400px'
                        style={{
                            ...ColStyle,
                            backgroundColor: '#37474f',
                            alignItems: 'center',
                            display: 'flex'
                        }}
                    >
                        <Slider
                            style={{
                                display: 'block',
                                height: height,
                                marginLeft: 30,
                                width: 'calc(100% - 60px)',
                            }}
                            {...SliderSettings}
                        >
                            {images?.map((imagedata, key) => (
                                <CustomSlide key={key} heightWrapper={height} cover={imagedata} />
                            ))}
                        </Slider>
                    </Col>
                    <Col
                        flex={'0 1 300px'}
                        style={ColStyle}
                    >
                        <div style={{ display: 'block' }} >
                            <Card
                                bordered={false}
                                title={<PostHeader owner={owner} createdAt={createdAt} />}
                                extra={[<CloseCircleFilled key='close-button' title="close" onClick={onClose}/>]}
                            >
                                <Skeleton paragraph={{ rows: 4 }} title active loading={loading}>
                                    <Typography.Title level={4}>
                                        {title}
                                    </Typography.Title>
                                    <Typography.Paragraph>
                                        {body}
                                    </Typography.Paragraph>
                                </Skeleton>
                            </Card>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default PostDetails;
