import React, { CSSProperties } from 'react';
import PropType from 'prop-types';
import Measure from 'react-measure';
import TweenOne from 'rc-tween-one';

type IAnimFlow = 'deploy' | 'backguard';

type EditorModalProps = {
    to: {
        top?: number;
        height?: number;
        width?: number;
    }
    autoplay?: boolean;
    animate: IAnimFlow;
    style?: CSSProperties;
    children?: React.ReactElement;
}

type IAnimState = {
    flow?: IAnimFlow;
    paused?: boolean;
    bounds?: ClientRect | null;
}

interface EditorModal {
    state: IAnimState;
    measurer: Measure;

    new(props: EditorModalProps): EditorModal;

    deploy(bounds: ClientRect): any;
    backguard(bounds: ClientRect): any;
    calc(contentRect: { bounds: ClientRect, client: ClientRect }): CSSProperties | {};
}

const at = (ay: number, f: number = 360): number => Math.abs(Math.ceil(ay * Math.sin(((2 * Math.PI) / f)) * 100));// amplitude of time
const ay = (q: number, p: number): number => (-1 * q) + p;// displacement on axis

const defaultStyle: CSSProperties = {
    display: 'block',
    position: 'inherit'
}

const defaultBounds: any = {
    top: 20,
};

function Helper(props): React.ReactElement {
    const { animate, animation, styledBounds } = props;

    return (
        <TweenOne
            key={animate}
            animation={animation}
            paused={false}
            style={{
                ...styledBounds,
                zIndex: 1000,
                display: 'flex',
                position: 'absolute',
                border: '1px solid red'
            }}
        >
            {props.children}
        </TweenOne>
    )
}

class EditorModal extends React.Component {
    static propTypes = {
        to: PropType.shape({
            bottom: PropType.number,
            height: PropType.number,
            width: PropType.number,
            right: PropType.number,
            left: PropType.number,
            top: PropType.number
        }),
        style: PropType.object,
        children: PropType.any,
        autoplay: PropType.bool
    }

    static defaultProps = {
        autoplay: true,
        animate: 'backguard',
        to: defaultBounds,
        style: defaultStyle
    }

    animation: any[] = [];
    state: IAnimState = {
        bounds: null
    }

    constructor(public props: EditorModalProps) {
        super(props);
        this.calc = this.calc.bind(this)
    }

    componentWillUpdate(next) {
        var { animate, autoplay } = next;

        if (autoplay) {
            console.log('measuring from :', animate, this.measurer.measure());
        }
    }

    deploy(bounds: ClientRect) {
        const { top, height, width, left } = bounds;

        const to_height = this.props.to.height || height;
        const to_width = this.props.to.width || width;
        const to_left = left + ((width - to_width) / 2);
        const to_top = this.props.to.top;

        const t1 = at(ay(top, height));
        const t2 = at(ay(height, to_height));

        return [
            {
                top, height,
                width, left,
                ease: 'easeInCubic'
            },
            { position: 'fixed' },
            {
                top: to_top,
                duration: t1,
                ease: 'easeInQuad'
            },
            {
                left: to_left,
                width: to_width,
                height: to_height,
                duration: t2,
                ease: 'easeOutSine'
            }
        ];
    }

    backguard(bounds: ClientRect) {
        var { top, height, width, left } = bounds;

        const from_height = this.props.to.height || height;
        const from_width = this.props.to.width || width;
        const from_left = left + ((width - from_width) / 2);

        const t1 = at(ay(top, height));
        const t2 = at(ay(height, from_height));

        return [
            {
                top: this.props.to.top,
                left: from_left,
                width: from_width,
                height: from_height,
                position: 'fixed'
            },
            {
                top,
                height,
                duration: t1,
                ease: 'easeOutSine'
            },
            {
                width, left,
                duration: t2,
                ease: 'easeInCubic'
            },
            {
                position: 'absolute',
                delay: t1,
            }
        ];
    }

    calc({ bounds }): CSSProperties {
        const { width, height, left, top } = bounds;
        console.log('calc bounds from: ', this.props.animate, this.animation);
        if (this.props.animate === 'backguard') {
            const from_width = this.props.to.width || width;

            return ({
                width: from_width,
                maxWidth: from_width,
                top: this.props.to.top,
                height: this.props.to.height || height,
                left: left + ((width - from_width) / 2)
            })
        }

        return { width, height, left, top };
    }

    render() {
        const { style, animate } = this.props;
        const { calc } = this;

        return (
            <Measure
                client bounds margin scroll
                ref={measurer => this.measurer = measurer}
                onResize={({ bounds }) => {
                    console.log('dispatch resising from:', animate, bounds);

                    this.animation = (animate === 'deploy') ?
                        this.deploy(bounds) :
                        this.backguard(bounds);
                }}
            >
                {({ measureRef, contentRect }) => (
                    <section ref={measureRef} className='animated-modal-ghost' style={{ ...defaultStyle, ...style }}>
                        <Helper styledBounds={calc(contentRect)} animate={animate} animation={this.animation} >

                        </Helper>
                    </section>
                )}
            </Measure>
        );
    }
}

export default EditorModal;