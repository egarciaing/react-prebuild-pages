import { CSSProperties } from 'react';
export type CoverData = {
    src: string;
    width: number;
    height: number;
    placeholder?: string;
};

export type Address = {
    email: string;
    verified?: boolean
}

export type Owner = {
    _id: number | string;
    username: string;
    firstNames: string;
    lastNames: string;
    fullName?: string;
    address: Address;
    aboutMe?: string;
    avatar?: string;
    company?: string;
    positionInCompany?: string;
}

export type ItemData<E = {}> = E & {
    _id: string | number;
    owner: Owner;
    title: string;
    createdAt: string;

    body?: string;
    cover?: CoverData;
    mediaCount?: number;
};

export type ItemProps<E = {}> = {
    data: ItemData<E>;
    width: number | string;
    style?:CSSProperties;
    measurer: () => void;
    onClick?: (params: ItemData<E>) => any;
}