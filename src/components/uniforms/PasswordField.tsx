import React from 'react';
import Input from 'antd/lib/input';
import wrapField from 'uniforms-antd/wrapField';
import { connectField, filterDOMProps } from 'uniforms';

const Password = props =>
    wrapField(
        props,
        <Input.Password
            disabled={props.disabled}
            id={props.id}
            name={props.name}
            onChange={event => props.onChange(event.target.value)}
            placeholder={props.placeholder}
            ref={props.inputRef}
            type={props.type}
            value={props.value}
            {...filterDOMProps(props)}
        />,
    );

Password.defaultProps = { visibilityToggle: true };

export default connectField(Password);