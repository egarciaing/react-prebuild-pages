import React from 'react';
import PropTypes from 'prop-types';
import { UserOutlined } from '@ant-design/icons';
import AntdAvatar from 'antd/es/avatar';

type AvatarProps = {
    avatar: {
        src: string
        username: string
    }
    size: 'small' | 'default' | 'large' | number
    icon: React.ReactElement
    dot: null | React.ReactElement
}

const dotStyle: React.CSSProperties = {
    zIndex: 2,
    display:'flex',
    flexFlow:'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
    position:'absolute',
    padding: 2,
    backgroundColor:'#fff' 
} 

function Avatar({ avatar, dot,...propsElem }: AvatarProps): React.ReactElement {
    const { username, src } = avatar;
    const [width, height] = typeof propsElem.size == 'string'? [24,24] : [propsElem.size * 0.5, propsElem.size * 0.5]; 
    const [left, top] = [`calc( 100% - ( ${width}px / 1.5 ))`,`calc( 100% - ( ${height}px / 1.5 ))`];

    return (
        <span style={{ display: 'inline-block', position:'relative', margin: "0 12px", zIndex:2 }}>
            <AntdAvatar src={src}  {...propsElem}>@{username}</AntdAvatar>
            { dot && <span style={{...dotStyle, width, height, left, top, borderRadius: width }} >{dot}</span> }
        </span>
    );
}
Avatar.propTypes = {
    avatar: PropTypes.shape({
        src: PropTypes.string,
        username: PropTypes.string,
    }).isRequired,
    size: PropTypes.oneOf(['small', 'default', 'large']),
    icon: PropTypes.object,
    dot: PropTypes.object
}
Avatar.defaultProps = {
    dot: null,
    size: 'small',
    icon: <UserOutlined />
}

export default Avatar;
