import React from 'react';
import { useSlate } from 'slate-react';
import PropTypes from 'prop-types';
import { Editor } from 'slate';
import { Button } from 'antd';
import {
    isBlockActive,
    isMarkActive,
    toggleBlock,
    toggleMark
} from './EditorTools';

interface EditorButtonProps {
    format: string
    Icon: React.ReactElement
    onMouseDown?: (editor: Editor, format: string) => void
}

const EditorButtonTypes = {
    format: PropTypes.string.isRequired,
    Icon: PropTypes.object.isRequired,
    onMouseDown: PropTypes.func
}

function Element({ attributes, children, element }) {
    switch (element.type) {
        case 'block-quote':
            return <blockquote {...attributes}>{children}</blockquote>
        case 'bulleted-list':
            return <ul {...attributes}>{children}</ul>
        case 'heading-one':
            return <h1 {...attributes}>{children}</h1>
        case 'heading-two':
            return <h2 {...attributes}>{children}</h2>
        case 'list-item':
            return <li {...attributes}>{children}</li>
        case 'numbered-list':
            return <ol {...attributes}>{children}</ol>
        case 'link':
            return (
                <a {...attributes} href={element.url}>
                    {children}
                </a>
            )
        default:
            return <p {...attributes}>{children}</p>
    }
}

function Leaf({ attributes, children, leaf }) {
    if (leaf.bold) {
        children = <strong>{children}</strong>
    }

    if (leaf.code) {
        children = <code>{children}</code>
    }

    if (leaf.italic) {
        children = <em>{children}</em>
    }

    if (leaf.underline) {
        children = <u>{children}</u>
    }

    return <span {...attributes}>{children}</span>;
}

function BlockButton(props: EditorButtonProps): React.ReactElement {
    const { format, Icon, onMouseDown } = props;
    const editor = useSlate();
    return (
        <Button
            onMouseDown={event => {
                event.preventDefault()
                !!onMouseDown ? onMouseDown(editor, format) : toggleBlock(editor, format);
            }}
            type={isBlockActive(editor, format) ? 'primary' : 'default'}
            style={{ borderColor: 'transparent', marginRight: 2 }}
        >
            {Icon}
        </Button>
    )
}
BlockButton.propTypes = EditorButtonTypes;

function MarkButton(props: EditorButtonProps): React.ReactElement {
    const { format, Icon, onMouseDown } = props;
    const editor = useSlate();
    return (
        <Button
            onMouseDown={event => {
                event.preventDefault()
                !!onMouseDown ? onMouseDown(editor, format) : toggleMark(editor, format);
            }}
            type={isMarkActive(editor, format) ? 'primary' : 'default'}
            style={{ borderColor: 'transparent', marginRight: 2 }}
        >
            {Icon}
        </Button>
    )
}
MarkButton.propTypes = EditorButtonTypes;

export { 
    BlockButton,
    MarkButton,
    Element,
    Leaf
}