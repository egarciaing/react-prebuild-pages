import { Editor, Transforms } from 'slate';
import { LIST_TYPES } from './EditorListUtils';

export const toggleBlock = (editor, format): void => {
    const isActive = isBlockActive(editor, format)
    const isList = LIST_TYPES.includes(format)
    
    Transforms.unwrapNodes(editor, {
        match: n => LIST_TYPES.includes(n.type),
        split: true,
    })

    Transforms.setNodes(editor, {
        type: isActive ? 'paragraph' : (isList ? 'list-item' : format),
    })

    if (!isActive && isList) {
        const block = { type: format, children: [] }
        Transforms.wrapNodes(editor, block)
    }
}

export const toggleMark = (editor, format): void => {
    const isActive = isMarkActive(editor, format)
    if (isActive) {
        Editor.removeMark(editor, format)
    } else {
        Editor.addMark(editor, format, true)
    }
}

export const isBlockActive = (editor, format): boolean => {
    const [match]: any = Editor.nodes(editor, {
        match: n => n.type === format,
    })

    return !!match
}

export const isMarkActive = (editor, format): boolean => {
    const marks = Editor.marks(editor)
    return marks ? marks[format] === true : false
}
