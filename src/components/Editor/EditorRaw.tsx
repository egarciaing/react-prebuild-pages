import React, { useState, useCallback, useMemo, CSSProperties } from 'react';
import PropType from 'prop-types';
import { createEditor } from 'slate';
import { Editable, withReact, Slate, } from 'slate-react';
import { withHistory } from 'slate-history'
import { Popover } from 'antd';
import {
    OrderedListOutlined,
    UnorderedListOutlined,
    BoldOutlined,
    ItalicOutlined,
    UnderlineOutlined,
    FontSizeOutlined,
    CodeOutlined,
    AlignLeftOutlined,
    LinkOutlined
} from '@ant-design/icons';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
    BlockButton,
    MarkButton,
    Element,
    Leaf
} from './EditorElements'
import { withLinks, insertLink } from './EditorLinkUtils'

type RichEditorProps = {
    value?: Array<any>;
    placeholder?: string;
    style?: CSSProperties;
    onChage?: (value: any[]) => void;
}

export const defaultValue = [
    {
        type: 'paragraph',
        children: [{ text: '' }],
    },
];

function RichEditor(props: RichEditorProps): React.ReactElement {
    const { value: initialValue, placeholder, style, onChage } = props;
    const [value, setValue] = useState<any[]>(initialValue as any[]);
    const renderElement = useCallback(params => <Element {...params} />, []);
    const renderLeaf = useCallback(params => <Leaf {...params} />, []);
    const editor = useMemo(() => withLinks(withHistory(withReact(createEditor()))), []);

    return (
        <section className='ritch-editor' style={style}>
            <Slate
                editor={editor}
                value={value}
                onChange={nextvalue => {
                    setValue(nextvalue);
                    onChage && onChage(nextvalue);
                }}
            >
                <div style={{ display: 'block', width: '100%', borderBottom:'1px solid #f0f0f0' }}>
                    <MarkButton format="bold" Icon={<BoldOutlined />} />
                    <MarkButton format="italic" Icon={<ItalicOutlined />} />
                    <MarkButton format="underline" Icon={<UnderlineOutlined />} />
                    <MarkButton format="code" Icon={<CodeOutlined />} />
                    <BlockButton format="heading-one" Icon={<FontSizeOutlined />} />
                    <BlockButton format="block-quote" Icon={<AlignLeftOutlined />} />
                    <BlockButton format="numbered-list" Icon={<OrderedListOutlined />} />
                    <BlockButton format="bulleted-list" Icon={<UnorderedListOutlined />} />
                    <Popover placement='bottom' trigger='clik'>
                        <BlockButton
                            format="link"
                            Icon={<LinkOutlined />}
                            onMouseDown={(editor) => {
                                insertLink(editor,'http://domain.xyz')
                                console.log(editor)
                            }}
                        />
                    </Popover>

                </div>
                <PerfectScrollbar
                    style={{
                        overflow: 'hidden',
                        flex: '1 1 auto',
                        width: '100%',
                    }}
                >
                    <Editable
                        renderElement={renderElement}
                        renderLeaf={renderLeaf}
                        placeholder={placeholder}
                        spellCheck
                        style={{
                            display: 'block',
                            width: '100%',
                            height:'100%'
                        }}
                        onFocus={() => {

                        }}
                    />
                </PerfectScrollbar>
            </Slate>
        </section>
    )
}
RichEditor.propTypes = {
    value: PropType.arrayOf(PropType.object).isRequired,
    placeholder: PropType.string,
    style: PropType.object,
};
RichEditor.defaultProps = {
    value: defaultValue,
    style: {
        width: 400,
        display: 'block'
    }
};

export default RichEditor;
