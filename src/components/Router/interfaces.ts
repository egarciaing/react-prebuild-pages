import React from 'react';

export interface route  {
    name : string 
    path : string | string[]
    exact?: boolean
    strict?: boolean
    sensitive?: boolean
    guard?: (props: any) => boolean 
    childrens?: route[]
    component: React.ReactElement | React.Component | any
}

export interface DynamicYieldProps {
    routes: Array<route>
    base: Array<string>
};
