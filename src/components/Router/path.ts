
const split = (path: string) => path.replace(/([/]{2,})/g, '/').split('/').filter(chunk => !!chunk);

const join = (path: string[]) => path.join('/').replace(/([/]{2,})/g, '/');

const fill = (path: string, data: { [key: string]: number | string | boolean  }): Array<number | string | boolean> => {
    const chunks = path.split(/\:([\w\_]+)/).filter(s => !!s) as Array<any>;
    const params = chunks.filter(s => !/^\/.*/.test(s));
    // replacing indexes
    for (const param of params) chunks[chunks.indexOf(param)] = data[param];
    
    return chunks
}

export { join, split, fill }
