import _ from 'underscore';
import { route } from './interfaces';

export default function filterDOMProps(props: route): { [key: string]: any } {
    return _.pick(props, 'exact', 'path', 'strict', 'sensitive');
}
