import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Redirect, RouteComponentProps } from 'react-router';

import { join } from './path';
import filterDOMProps from './filterDOMProps';
import { route, DynamicYieldProps } from './interfaces';

function Yield({ routes, base }: DynamicYieldProps): React.ReactElement {

    const segment = routes.map<React.ReactElement>((chunk: route) => {
        const { name, guard, childrens } = chunk;

        const is404 = chunk.path === '' && base.length === 0;
        const isRoot = chunk.path === '/' && base.length === 0;
        const isTree = childrens instanceof Array && childrens.length > 0;

        const props = filterDOMProps(chunk) as route;
        props.path = ((isRoot || is404) ? [props.path] : base.concat(props.path)) as string[];
        props.path = typeof props.path === 'string' ? props.path : join(props.path);

        var estrategy: string = 'component';
        var payload: (current: RouteComponentProps) => JSX.Element | JSX.Element;

        if (guard && !guard({ name, isTree, is404, isRoot })) {
            estrategy = 'render';
            payload = (current: RouteComponentProps) => <Redirect to={{ pathname: '/unauthorized', state: { from: current.location } }} />;
        } else {
            payload = isTree ? <Yield routes={childrens as route[]} base={[props.path]} /> : chunk.component;
        }

        return <Route key={name} {...props} {...{ [estrategy]: payload }} />
    });

    return <Switch>{segment}</Switch>
}
Yield.propTypes = {
    routes: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            path: PropTypes.oneOf([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]).isRequired,
            children: PropTypes.arrayOf(PropTypes.object),
            component: PropTypes.object,
        })
    ).isRequired,
    base: PropTypes.arrayOf(PropTypes.string),
}
Yield.defaultProps = {
    base: []
}

export default Yield