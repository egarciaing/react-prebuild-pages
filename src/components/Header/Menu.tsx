import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'antd/es/popover';
//import Button from 'antd/es/button';
import MenuAntd from 'antd/es/menu';
import Typography from 'antd/es/typography';
import { useHistory, useLocation } from 'react-router';

import Avatar from '../Avatar';

type MenuProps = {
    user?: {
        [key: string]: any
        avatar: {
            uid: string
            src: string
            username: string
        }
    } | null
    children?: React.ReactElement | null
    routes: Array<{ label: string, path: string }>
}

function Menu({ user, routes, children }: MenuProps): React.ReactElement {
    const History = useHistory();
    const Location = useLocation();

    return (
        <div className="menu" style={{ display: 'contents' }}>
            <span className="menu-links" style={{ display: 'inline-block', alignSelf: 'flex-end' }}>
                <MenuAntd onClick={({ key }) => History.push(key)} selectedKeys={[Location.pathname]} mode='horizontal'>
                    {routes?.map(({ label, path }) => (
                        <MenuAntd.Item key={path} >{label}</MenuAntd.Item>
                    ))}
                </MenuAntd>
            </span>
            {user &&
                <Popover
                    content={children}
                    placement="bottomRight"
                    title={<Typography.Text strong ellipsis>@{user?.username}</Typography.Text>}
                    trigger="click"
                >
                    <Avatar avatar={user.avatar} size='large' />
                </Popover>
            }
        </div>
    );
}
Menu.propTypes = {
    user: PropTypes.object,
    children: PropTypes.object,
    routes: PropTypes.array.isRequired
}
Menu.defaultProps = {
    routes: []
}

export default Menu;
