import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'antd/es/typography'
import { ApiFilled } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import Avatar from '../Avatar';

type HeaderProps = {
    baseUrl: string
    iconUrl: string
    title: string
    height?: number
    children?: JSX.Element
}

const headerStyle: React.CSSProperties = {
    width: '100%',
    backgroundColor: '#fff',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignContent: 'center'
}

const headerSectionsStyle: React.CSSProperties = {
    display: 'inherit',
    alignItems: 'center',
    position: 'relative'
}

function Header({ baseUrl, title, iconUrl, height, children }: HeaderProps): React.ReactElement {
    return (
        <div className='header__container' style={{ ...headerStyle, height }}>

            <Link to={baseUrl} className="header__container-title" style={headerSectionsStyle}>
                <Avatar avatar={{ src: iconUrl, username: title }} size='large' icon={<ApiFilled />} />
                <Typography.Title underline level={3} >{title}</Typography.Title>
            </Link>
            <section className="header__container-main" style={{ flex: '1 1 auto', display: 'inline-block', margin: '0 1rem' }}>
                {/* Main header */}
            </section>
            <section className='header__container-menu' style={headerSectionsStyle}>
                {children}
            </section>
        </div>
    )
}
Header.propTypes = {
    baseUrl: PropTypes.string.isRequired,
    iconUrl: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    height: PropTypes.number
}
Header.defaultProps = {
    height: 64,
    baseUrl: '/'
}

export default Header;
