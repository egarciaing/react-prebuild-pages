import React, { CSSProperties } from 'react';
import Moment from 'react-moment';
import MomentTypes from 'react-moment-proptypes';
import PropTypes from 'prop-types';
import { Avatar, Typography } from 'antd';
import { UserOutlined, EditFilled } from '@ant-design/icons'
import { Owner } from '../CommonTypes';

type PostHeaderProps = {
    owner: Owner
    createdAt?: string
    titleSize?: number
    avatarSize?: 'default' | 'large' | 'small' | number
    showUsername?: boolean
    aboutMe?: boolean
    style?: CSSProperties
    actions?: React.ReactElement[]
}

const defaultStyle: CSSProperties = {
    display: 'block',
    width: '100%',
    padding: 10
}

function PostHeader(props: PostHeaderProps): React.ReactElement {
    const { owner, actions, createdAt, showUsername, aboutMe: showAboutMe, avatarSize, titleSize, style } = props;
    const { fullName, username, avatar, aboutMe } = owner;

    const maxBodyWidth = `calc(100% - ${typeof avatarSize === 'string' ? 40 : (avatarSize as number + 8)}px)`;

    return (
        <div style={{ ...defaultStyle, ...style }}>
            <Avatar
                size={avatarSize}
                style={{ verticalAlign: 'top', marginRight: 5 }}
                icon={<UserOutlined />}
                alt={`@${username}`}
                src={avatar}
            > {username} </Avatar>
            <div style={{ display: 'inline-block', maxWidth: maxBodyWidth }}>
                <Typography.Text title={fullName} strong style={{ fontSize: titleSize, maxWidth: '100%', display: 'block' }} ellipsis>{fullName || username}</Typography.Text>
                <Typography.Text style={{ fontSize: 12, maxWidth: '100%' }} >
                    {showUsername ? `@${username} ` : null}
                    <span>
                        <EditFilled style={{ marginLeft: 10 }} />
                        {createdAt && <Moment date={createdAt} fromNow ago />}
                    </span>
                </Typography.Text>
                {
                    showAboutMe ? (
                        <Typography.Paragraph type='secondary' style={{ marginTop: 20 }}>
                            {aboutMe}
                        </Typography.Paragraph>
                    ) : null
                }
                <div className='profile-header-footer'>
                    {actions}
                </div>
            </div>
        </div>
    )
}
PostHeader.propTypes = {
    owner: PropTypes.object.isRequired,
    createdAt: MomentTypes.momentString,
    avatarSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    showUsername: PropTypes.bool,
    aboutMe: PropTypes.bool,
    style: PropTypes.object,
    actions: PropTypes.array
}
PostHeader.defaultProps = {
    actions: [],
    avatarSize: 'default',
    titleSize: 12,
    showUsername: false,
    aboutMe: false,
    style: defaultStyle
}

export default PostHeader;