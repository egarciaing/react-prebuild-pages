import React, { CSSProperties } from 'react';
import PropTypes from 'prop-types';
import {
    Avatar,
    Typography,
    Card, Divider
} from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Owner } from '../CommonTypes';

type ProfileCardProps = {
    owner: Owner,
    titleSize?: number
    avatarSize?: 'default' | 'large' | 'small' | number
    showUsername?: boolean
    aboutMe?: boolean
    style?: CSSProperties
}

interface ProfileCard {
    new(props: ProfileCardProps): ProfileCard
}
const defaultStyle: CSSProperties = {
    overflow:'hidden',
    borderRadius: '10px',
    display: 'block',
    width: 300
}

class ProfileCard extends React.Component {
    static defaultProps = {
        avatarSize: 100,
        titleSize: 14,
        showUsername: true,
        aboutMe: true,
        style: defaultStyle
    }

    static propTypes = {
        owner: PropTypes.object.isRequired,
        avatarSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        showUsername: PropTypes.bool,
        aboutMe: PropTypes.bool,
        style: PropTypes.object
    }

    constructor(public props: ProfileCardProps) {
        super(props);
    }

    render() {
        const { titleSize, avatarSize, owner, style } = this.props;
        const { _id, fullName, username, aboutMe, avatar } = owner;

        return (
            <Card key={_id} style={{ ...defaultStyle, ...style}} hoverable bordered>
                <div style={{ display: 'flex', alignItems: 'center', flexFlow: 'column nowrap' }}>
                    <Avatar
                        size={avatarSize}
                        style={{ verticalAlign: 'top', margin: 5 }}
                        icon={<UserOutlined />}
                        alt={`@${username}`}
                        src={avatar}
                    > {username} </Avatar>
                    <Typography.Text strong style={{ display: 'block', fontSize: titleSize }}>
                        {fullName}
                    </Typography.Text>
                    <Divider type='horizontal' orientation='center' >
                        <Typography.Text style={{ display: 'block', fontSize: '0.854rem' }}>
                            {`@${username}`}
                        </Typography.Text>
                    </Divider>
                </div>
                <Typography.Paragraph type='secondary' style={{ marginTop: 20 }} ellipsis={{ rows: 5, expandable: true }}>
                    {aboutMe}
                </Typography.Paragraph>
                <div className='card-footer' style={{ margin:'0 -1.5rem -1.5rem' }}>
                    <button>Hola mundo</button>
                </div>
            </Card>
        )
    }
}

export default ProfileCard;