import React, { CSSProperties } from 'react';
import PropTypes from 'prop-types';
import { Avatar, Typography } from 'antd';
import { UserOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Owner } from '../CommonTypes';

type ProfileBadgeProps = {
    owner: Owner
    style?: CSSProperties
    useRealName?: boolean
    removable?: boolean
    onRemove?: () => void
}

const defaultStyle: CSSProperties = {
    padding: 5,
    borderRadius: 50,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#f0f0f0',
    overflow: 'hidden',
    minWidth: 180,
    maxWidth: 220,

    display: 'inline-flex',
    flexFlow: 'row nowrap',
    justifyContent: 'center',
    alignItems: 'center',
}

function ProfileBadge(props: ProfileBadgeProps): React.ReactElement {
    const { style, owner, useRealName, removable, onRemove } = props;
    const { avatar, username, fullName } = owner;

    return (
        <div className='profile-badge' style={{ ...defaultStyle, ...style }}>
            <div style={{ display: 'inline-block' }}>
                <Avatar src={avatar} icon={<UserOutlined />} alt={username} size='small' style={{ marginRight: 5 }}>{username}</Avatar>
            </div>
            <Typography.Text strong type='secondary' style={{ flex: '1 1 180px' }} ellipsis>{useRealName ? fullName : `@${username}`}</Typography.Text>
            {removable ? <CloseCircleOutlined onClick={onRemove} /> : null}
        </div>
    );
}
ProfileBadge.propTypes = {
    owner: PropTypes.object.isRequired,
    useRealName: PropTypes.bool,
    removable: PropTypes.bool,
    onRemove: PropTypes.func,
    style: PropTypes.object
}
ProfileBadge.defaultProps = {
    useRealName: false,
    style: defaultStyle,
    removable: false,
}

export default ProfileBadge;