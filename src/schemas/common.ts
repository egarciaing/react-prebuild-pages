import SimpleSchema from "simpl-schema";
import { nothing } from 'uniforms'

const common = new SimpleSchema({
    createdAt: {
        type: Date,
        optional: true,
        uniforms: () => nothing,
        autoValue() {
            if (this.isInsert) {
                return new Date
            } else if (this.isUpsert) {
                return { $setOnInsert: new Date }
            } else {
                this.unset()
            }
        },

    },
    updatedAt: {
        type: Date,
        optional: true,
        uniforms: () => nothing,
        autoValue() {
            if (this.isUpdate) {
                return new Date
            } else {
                this.unset()
            }
        }
    }
});

export default common
