import SimpleSchema from "simpl-schema";
import BridgeSchema from "uniforms-bridge-simple-schema-2";
import { PasswordField } from '../../components/uniforms';

const changePassword = new SimpleSchema({
    currentPassword: {
        type: String,
        min: 8,
        uniforms:{
            component:PasswordField
        }
    },
    newPassword: {
        type: String,
        min: 8,
        uniforms:{
            component:PasswordField
        }
    },
    confirmPassword: {
        type: String,
        min: 8,
        uniforms:{
            component:PasswordField
        },
        custom() {
            if (this.value !== this.field('newPassword').value) {
                return "passwordMismatch";
            }
        },
    },
});

const changePasswordForm = new BridgeSchema(changePassword);

export default changePassword
export {
    changePasswordForm
}
