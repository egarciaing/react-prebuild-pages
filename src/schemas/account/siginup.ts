import SimpleSchema from "simpl-schema";
import BridgeSchema from "uniforms-bridge-simple-schema-2";
import { PasswordField } from '../../components/uniforms';

const siginUp = new SimpleSchema({
    username: {
        type: String,
        regEx: /^[a-z0-9A-Z_]{3,15}$/,
        //unique: true, // only in MeteorJS with [aldeed:schema-index] package
        custom() {
            if (this.isSet) {
                /**
                 * CODE HERE
                 * this.validationContext.addValidationErrors([{
                 *  name: "username",
                 *  type: "userNameNotUnique"
                 * }]);
                 */
            }
        }
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        //unique: true, // only in MeteorJS with [aldeed:schema-index] package
        uniforms: {
            type: "email"
        },
        custom() {
            if (this.isSet) {
                /**
                 * CODE HERE
                 * this.validationContext.addValidationErrors([{
                 *  name: "username",
                 *  type: "userNameNotUnique"
                 * }]);
                 */
            }
        }
    },
    password: {
        type: String,
        min: 8,
        uniforms: {
            component: PasswordField
        }
    },
    confirmPassword: {
        type: String,
        min: 8,
        uniforms: {
            component: PasswordField
        },
        custom() {
            if (this.value !== this.field('password').value) {
                return "passwordMismatch";
            }
        },
    },
    policity: {
        type: Boolean,
        label:''
    }
});

const siginupForm = new BridgeSchema(siginUp);

export default siginUp
export {
    siginupForm
}
