import SimpleSchema from "simpl-schema";
import BridgeSchema from "uniforms-bridge-simple-schema-2";
import LongTextField from 'uniforms-antd/LongTextField';

const profile = new SimpleSchema({
    firstNames: {
        type: String
    },
    lastNames: {
        type: String,
        optional: true
    },
    webSite: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: true
    },
    phoneNumber: {
        type: String,
        regEx: SimpleSchema.RegEx.Phone,
        optional: true
    },
    sex: {
        type: String,
        allowedValues: ['Male', 'Female']
    },
    biography: {
        type: String,
        uniforms: {
            component: LongTextField
        },
        optional: true
    }
});

const profileForm = new BridgeSchema(profile);

export default profile;
export {
    profileForm
}
