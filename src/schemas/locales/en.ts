export default {
    passwordMismatch: 'Passwords do not match',
    userNameNotUnique: 'Username unavailable'
};
