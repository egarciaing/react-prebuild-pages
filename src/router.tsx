import * as Account from './Pages/Account';
import LandingPage from './Pages/Landing';
import Posts from './Pages/Posts';
import Setup from './Pages/PageSetup';

import { route } from './components/Router/interfaces';

const Router: route[] = [
    { name: 'landing', path: '/', exact: true, component: LandingPage },
    { name: 'loginIn', path: '/login', component: Account.LoginIn },
    { name: 'siginUp', path: '/sigin-up', component: Account.SiginUp },
    { name: 'verifyEmail', path: '/verify-email/:token', component: Account.VerifyEmail },
    { name: 'forgotPassword', path: '/forgot-password', component: Account.ForgotPassword },
    { name: 'accountCreated', path: '/account-created', component: Account.Created },

    { name: 'pageSetup', path: '/page-setup', component: Setup },
    { name: 'posts', path: '/publications', component: Posts },

];

export default Router;
