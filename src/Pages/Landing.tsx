import React from 'react';
import PropTypes from 'prop-types';

class Landing extends React.Component {
    static propTypes = {
        title: PropTypes.string,
        logo: PropTypes.string,
        description: PropTypes.string
    }

    render(){
        return <div> Landing Page </div>
    }
}

export default Landing;
