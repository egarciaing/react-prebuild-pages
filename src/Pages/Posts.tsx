import React from 'react';

import Layout from 'antd/es/layout';
import Row from 'antd/es/row';
import Col from 'antd/es/col';
import Divider from 'antd/es/divider';
import Buttom from 'antd/es/button';
import { SendOutlined } from '@ant-design/icons';

import { ItemData } from '../components/CommonTypes';
import List from '../components/Post/PostList';
import Item from '../components/Post/PostItem';

import Editor, { defaultValue } from '../components/Editor/EditorRaw';

import { item, item2 } from '../fake/api';

const {
    Header,
    Content
} = Layout;

class Posts extends React.Component {
    state = {
        hasMore: 2,
        overscan: 15
    }

    constructor(props) {
        super(props);

        this.fetch = this.fetch.bind(this);
        this.onSelectItem = this.onSelectItem.bind(this);
    }

    fetch() {
        return Promise.resolve([]);
    }

    onSelectItem(/*{index: number, loading: boolean, data: ItemData}*/) {

    }

    list: ItemData[] = [item2, item, item2];

    render() {
        return (
            <Layout>
                <Header>header</Header>
                <Content >
                    <Row justify='center' align='top' style={{ maxWidth: 1400, margin: 'auto' }} >
                        <Col xs={{ span: 0, offset: 0 }} sm={{ span: 6, offset: 0 }} lg={{ span: 5, offset: 0 }}></Col>
                        <Col xs={{ span: 24, offset: 0 }} sm={{ span: 18, offset: 0 }} lg={{ span: 12, offset: 1 }}>
                            <div
                                style={{
                                    width: 500,
                                    height: 400,
                                    margin: 'auto',
                                    marginBottom: 20,
                                    marginTop: 20,
                                    padding: 10,
                                    display: 'flex',
                                    flexFlow: 'column nowrap',
                                    backgroundColor: '#fff'
                                }}
                            >
                                <Editor value={defaultValue} style={{ flex: '1 1 auto' }} />
                                <div style={{ height: 50, marginTop: 10 }}>
                                    <Buttom type='primary' size='large' style={{ width: '100%' }}>Publish <SendOutlined /></Buttom>
                                </div>
                            </div>
                            <Divider className='custom-divider'> Publicaciones </Divider>
                            <List
                                list={this.list}
                                loadMoreHandler={this.fetch}
                                onItemClick={this.onSelectItem}
                                renderItem={({ index, ...props }) => <Item key={index} {...props} />}
                                hasMore={this.state.hasMore}
                                overscan={this.state.overscan}
                                style={{
                                    width: 500,
                                    height: 700,
                                    margin: 'auto'
                                }}
                            />
                        </Col>
                        <Col sm={{ span: 0, offset: 0 }} lg={{ span: 5, offset: 1 }}></Col>
                    </Row>
                </Content>
            </Layout>
        )
    }
}

export default Posts;