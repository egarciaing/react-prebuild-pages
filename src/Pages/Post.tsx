import React from 'react';
import PropTypes from 'prop-types';

class Post extends React.Component {
    static propTypes = {
        _id: PropTypes.string.isRequired
    }
}

export default Post;
