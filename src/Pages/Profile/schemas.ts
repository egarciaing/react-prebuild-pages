import SimpleSchema from 'simpl-schema';

const profile = new SimpleSchema({
    name: String
})

export { profile }