import React from 'react';
import Card from "antd/es/card";
import Form, { FormInstance } from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import Checkbox from "antd/es/checkbox";
import Divider from "antd/es/divider";
import Typography from "antd/es/typography";
import { Link } from "react-router-dom";
import {
    UserOutlined,
    LockOutlined,
    CloseCircleOutlined
} from "@ant-design/icons";
import { titleStyle } from "./layout";

const formStyle: React.CSSProperties = {
    maxWidth: 300,
    width: '100%',
    margin: 'auto'
};

class LoginIn extends React.Component {
    form: FormInstance | null = null;

    state = {
        messageError: null
    }

    registerRef = (ref: FormInstance | null) => {
        this.form = ref;
    }

    onLoginIn = (model: { email: string, password: string }) => {
        this.setState({ messageError: JSON.stringify(model) })// <- fake
        // CODE HERE
    }

    render() {
        const { messageError } = this.state;
        return (
            <div className="center-container" >
                <Card
                    style={formStyle}
                    hoverable={true}
                    bordered={true}
                    title={<Typography.Title level={4} style={titleStyle}>Sign In</Typography.Title>}
                >
                    <Form
                        name="login"
                        size="large"
                        layout="vertical"
                        ref={this.registerRef}
                        onFinish={this.onLoginIn as any}
                    >
                        <Form.Item
                            name="email"
                            rules={[{ required: true, message: 'Please input your Email!' }]}
                        >
                            <Input prefix={<UserOutlined />} placeholder="email" type="email" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please input your Password!' }]}
                        >
                            <Input.Password prefix={<LockOutlined />} placeholder="Password" />
                        </Form.Item>
                        <Form.Item>
                            <Form.Item name="remember" valuePropName="checked" noStyle>
                                <Checkbox>Remember me</Checkbox>
                            </Form.Item>
                            <Link to={"/forgot-password"} style={{ float: 'right' }}>Forgot password</Link>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" role="button" size="large" style={{ width: '100%' }} >Submit</Button>
                        </Form.Item>
                        <Divider>Or</Divider>
                        <Form.Item>
                            <Link to={"/sigin-up"}  >
                                <Button type="ghost" htmlType="button" size="large" style={{ width: '100%' }}>Sigin Up</Button>
                            </Link>
                        </Form.Item>

                        {!!messageError &&
                            <Form.Item>

                                <Typography.Paragraph style={{ color: 'red' }}>
                                    <CloseCircleOutlined /> {messageError}
                                </Typography.Paragraph>

                            </Form.Item>
                        }
                    </Form>
                </Card>
            </div>
        );
    }
}

export default LoginIn;
