import LoginIn from './LoginIn';
import SiginUp from './SiginUp';
import VerifyEmail from './VerifyEmail';
import ForgotPassword from './ForgotPassword';
import Created from './Successfully';

export { 
    LoginIn,
    SiginUp,
    VerifyEmail,
    ForgotPassword,
    Created
}
