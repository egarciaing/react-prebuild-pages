import { CSSProperties } from 'react';

const layoutStyle: CSSProperties = {
    maxWidth: 300,
    width: '100%',
    margin: 'auto'
};

const titleStyle: CSSProperties = { 
    textAlign: 'center', 
    display: 'block', 
    width: '100%' 
};

const layout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

export default layout;
export {
    layoutStyle,
    titleStyle
}