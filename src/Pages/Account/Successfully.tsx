import React from 'react';
import Result from 'antd/es/result';
import Typography from "antd/es/typography";

class Successfully extends React.Component {
    render() {
        return (
            <div className="center-container">
                <Result 
                    status="success"
                    title="Account created"
                >
                    <Typography.Title type="secondary" level={4}>
                        Verify your email
                    </Typography.Title>
                </Result>
            </div>
        )
    }
}

export default Successfully;
