import React from 'react';
import Result from 'antd/es/result';
import Button from 'antd/es/button';
import Input from "antd/es/input";
import Form from "antd/es/form";
import { Link, RouteComponentProps } from "react-router-dom";
import { LoadingOutlined, MailOutlined } from "@ant-design/icons";

class VerifyEmail extends React.Component {

    state = {
        status: "loading",
        title: "Please wait...",
        iconSize: 64,
        subTitle: null,
        email: null
    }

    token: string | null = null;

    componentWillMount() {
        const { match } = this.props as RouteComponentProps;
        this.checkToken(match.params as any)
    }

    onTryAgain = ({ email }: any): void => {
        const oldToken = this.token;
        console.log(email, oldToken);
        // fetch to server
        // CODE HERE
    }

    checkToken({ token }: { token: string | never }) {
        
        const mapper = (result: string, message: string | null) => {
            switch (result) {
                case 'success':
                    this.setState({ status: result, title: "Go to you Profile." });
                    break;
                case 'info':
                    this.setState({ status: result, title: message });
                    break;
                case 'error':
                    this.setState({ status: result, title: message });
                    break;
                default:
                    this.setState({ status: "warning", title: "Upps... an error ocurred.", subTitle: "Try resend email." });
            };
        }

        if (token) {
            this.token = token;
            // fetch to server
            // CODE HERE
            mapper('tryAgain', null);// <- fake
            return;
        }

        this.setState({ status: "error", title: "No match token" });
    }

    render() {
        const { status, title, iconSize, subTitle } = this.state;

        return (
            <Result
                status={status !== "loading" && status as any}
                icon={status === "loading" && <LoadingOutlined spin style={{ fontSize: iconSize }} />}
                title={title}
                subTitle={subTitle}
                extra={
                    status === "success" ? ([
                        <Link key="profile" to="/profile" > <Button type="primary"> Go Profile </Button> </Link>,
                        <Link key="post" to="/posts" > <Button >Posts</Button> </Link>,
                    ]) :
                        !!subTitle ?
                            [
                                <Form
                                    key="verification_tryagain"
                                    name="verification_tryagain"
                                    layout="inline"
                                    style={{ display: 'inline-flex' }}
                                    onFinish={this.onTryAgain}
                                >
                                    <Form.Item
                                        name="email"
                                        rules={[{ required: true, message: 'Please input your Email!' }]}
                                    >
                                        <Input
                                            type="email"
                                            placeholder={"Email"}
                                            prefix={<MailOutlined />}
                                            style={{ width: "100%", maxWidth: 300 }}
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <Button htmlType="submit" type="primary">Try again</Button>
                                    </Form.Item>
                                </Form>
                            ] :
                            null
                }
            />
        )
    }
}

export default VerifyEmail;
