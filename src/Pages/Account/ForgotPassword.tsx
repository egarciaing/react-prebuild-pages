import React from "react";
import Card from "antd/es/card";
import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import { MailOutlined } from "@ant-design/icons";
import { titleStyle, layoutStyle } from "./layout";

class ForgotPassword extends React.Component{
    render(){
        return (
            <div className="center-container">
                <Card
                    style={layoutStyle}
                    hoverable={true}
                    bordered={true}
                    title={<div style={titleStyle}>Forgot password</div>}
                >
                    <Form
                        size="large"
                        layout="vertical"
                        name="login"
                    >
                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[{ required: true, message: 'Please input your Email!' }]}
                        >
                            <Input prefix={<MailOutlined />} placeholder="email" />
                        </Form.Item>
                       
                        <Form.Item>
                            <Button type="primary" htmlType="submit" role="button" size="large" style={{ width: '100%' }} >Submit</Button>
                        </Form.Item>
                    </Form>
                </Card>
            </div>
        )
    }
}

export default ForgotPassword;