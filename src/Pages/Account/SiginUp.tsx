import React from 'react';
import Card from "antd/es/card";
import Form from "antd/es/form";
import Typography from "antd/es/typography";
import { AutoForm, AutoFields, AutoField, SubmitField } from "uniforms-antd";
import { Link, RouteComponentProps } from "react-router-dom";

import { siginupForm } from "../../schemas/account/siginup";
import { layoutStyle, titleStyle } from "./layout";

class SignUp extends React.Component {
    onSiginUp = (model: any) => {
        const { history } = this.props as RouteComponentProps;
        history.push("/account-created");
        console.log(model)
        // CODE HERE
    }

    render() {
        return (
            <div className="center-container">
                <div style={{ ...layoutStyle, marginTop: 12, marginBottom: 12 }}>
                    <Card
                        title={<Typography.Title level={4} style={titleStyle} >Register</Typography.Title>}
                        hoverable={true}
                        bordered={true}

                    >
                        <AutoForm
                            className="ant-form ant-form-vertical"
                            schema={siginupForm}
                            onSubmit={this.onSiginUp}
                        >
                            <AutoFields
                                omitFields={['policity']}
                                autoField={({ name, ...rest }) => {
                                    return <AutoField name={name} {...rest} />
                                }}
                            />
                            <Form.Item>
                                <div style={{ display: 'inline-block', marginRight: 12 }}>
                                    <AutoField name="policity" />
                                </div>
                                <div style={{ display: 'inline-block' }}>
                                    <Typography.Paragraph>
                                        policity
                                </Typography.Paragraph>
                                </div>
                            </Form.Item>
                            <SubmitField size={'large'} style={{ maxWidth: '100%', width: 200, display: 'block', margin: 'auto' }} />
                        </AutoForm>
                    </Card>
                    <p style={{ margin: "auto", textAlign: 'center' }}>
                        <span> Already have login and password? </span> <Link to={'/login'}>Sign in</Link>
                    </p>
                </div>
            </div>
        );
    }
}

export default SignUp
